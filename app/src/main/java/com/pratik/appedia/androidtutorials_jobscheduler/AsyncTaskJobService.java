package com.pratik.appedia.androidtutorials_jobscheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

public class AsyncTaskJobService extends JobService {

    private static final String TAG = AsyncTaskJobService.class.getSimpleName();
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG,"inside onStartJob");
        new SleepingTask(this).execute(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) { return false;
    }

    private static class SleepingTask extends AsyncTask<JobParameters, Void, JobParameters>{

       private final JobService jobService;

        private SleepingTask(JobService jobService) {
            this.jobService = jobService;
        }

        @Override
        protected JobParameters doInBackground(JobParameters... jobParameters) {
            Log.d(TAG,"inside doInBackground");
            SystemClock.sleep(5000);
            return jobParameters[0];
        }

        @Override
        protected void onPostExecute(JobParameters jobParameters) {
            Log.d(TAG,"inside onPostExecute");
            Toast.makeText(jobService.getApplicationContext(),"Job finished execution",Toast.LENGTH_SHORT).show();
            jobService.jobFinished(jobParameters,false);
        }
    }
}
