package com.pratik.appedia.androidtutorials_jobscheduler;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import static android.support.v4.app.NotificationCompat.PRIORITY_HIGH;

public class NotificationJobService extends JobService {

    NotificationManager mNotifyManager;

    //Notification channel ID.
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";

    /**
     * Creates notification channel for OREO and higher
     */
    public void createNotificationChannel(){
        //Define notification manager object
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //Notification channels are only for OREO and higher
        //So need to check the SDK version
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            //Create notification channel with all the parameters
            NotificationChannel  notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID,
                    "Job Service notification",NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notifications from Job Service");

            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        //Create the notification channel
        createNotificationChannel();

        //Set up notification content intent to launch the app when it gets clicked
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this,0,
                new Intent(this,MainActivity.class)
                ,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                PRIMARY_CHANNEL_ID)
                .setAutoCancel(true)
                .setContentTitle("Job Service")
                .setContentText("Your job ran to completion!")
                .setContentIntent(contentPendingIntent)
                .setSmallIcon(R.drawable.ic_job_running)
                .setPriority(PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL);
        mNotifyManager.notify(0,builder.build());
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
